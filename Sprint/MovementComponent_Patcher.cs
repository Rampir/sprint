﻿using Harmony;
using UnityEngine;
using System.Collections;
using System;

namespace Sprint
{
    [HarmonyPatch(typeof(MovementComponent))]
    [HarmonyPatch("UpdateMovement")]
    internal class MovementComponent_UpdateMovement_Patch
    {
        [HarmonyPostfix]
        public static void Postfix(MovementComponent __instance)
        {

            bool toggleMode = Configuration.getInstance().getToggleMode();
            if (toggleMode)
            {
                if (__instance.wgo.is_player)
                {
                    if (MainGame.me.player.GetParam("isSprinting") == 1)
                    {
                        __instance.SetSpeed(7.0f);
                        MainGame.me.player.energy -= 0.04f;
                    }
                    else
                    {
                        __instance.SetSpeed(3.0f);
                    }
                }
            }

            else
            {

                if (Input.GetKey(KeyCode.LeftShift))
                {

                    if (__instance.wgo.is_player)
                    {
                        __instance.SetSpeed(7.0f);
                        MainGame.me.player.energy -= 0.04f;
                    }
                }
                if (!Input.GetKey(KeyCode.LeftShift))
                {
                    if (__instance.wgo.is_player)
                    {
                        __instance.SetSpeed(3.0f);
                    }
                }
            }


        }


    }
}